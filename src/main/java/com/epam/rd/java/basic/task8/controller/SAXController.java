package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.enitity.Flower;
import com.epam.rd.java.basic.task8.exception.XmlException;
import com.epam.rd.java.basic.task8.util.FlowerXmlNamespace;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements XMLParser<Flower> {

    private String xmlFileName;
    private List<Flower> flowers;
    private StringBuilder elementValue;
    private Flower.Measure<Integer> measureAttribute;
    private Flower.FlowerBuilder flowerBuilder;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public void startDocument() {
        flowers = new ArrayList<>();
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (elementValue == null) {
            elementValue = new StringBuilder();
        } else {
            elementValue.append(ch, start, length);
        }
    }

    @Override
    public void startElement(String uri, String lName, String qName, Attributes attr) {
        switch (FlowerXmlNamespace.fromString(qName)) {
            case FLOWER:
                flowerBuilder = new Flower.FlowerBuilder();
                break;

            case NAME:
            case SOIL:
            case ORIGIN:
            case STEM_COLOUR:
            case LEAF_COLOUR:
            case MULTIPLYING:
                elementValue = new StringBuilder();
                break;

            case AVE_LEN_FLOWER:
            case TEMPERATURE:
            case WATERING:
                elementValue = new StringBuilder();
                measureAttribute = new Flower.Measure<>(attr.getValue(0), 0);
                break;

            case LIGHTING:
                elementValue = new StringBuilder(attr.getValue(0));
                break;

            default:
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (FlowerXmlNamespace.fromString(qName)) {
            case FLOWER:
                flowers.add(flowerBuilder.build());
                break;

            case NAME:
                flowerBuilder.withName(elementValue.toString());
                break;

            case SOIL:
                flowerBuilder.withSoil(Flower.Soil.fromString(elementValue.toString()));
                break;

            case ORIGIN:
                flowerBuilder.withOrigin(elementValue.toString());
                break;

            case STEM_COLOUR:
                flowerBuilder.withStemColour(elementValue.toString());
                break;

            case LEAF_COLOUR:
                flowerBuilder.withLeafColour(elementValue.toString());
                break;

            case AVE_LEN_FLOWER:
                measureAttribute.setValue(Integer.valueOf(elementValue.toString()));
                flowerBuilder.withAveLenFlower(measureAttribute);
                break;

            case TEMPERATURE:
                measureAttribute.setValue(Integer.valueOf(elementValue.toString()));
                flowerBuilder.withTemperature(measureAttribute);
                break;

            case LIGHTING:
                flowerBuilder.withLighting(Flower.Lighting.fromString(elementValue.toString()));
                break;

            case WATERING:
                measureAttribute.setValue(Integer.valueOf(elementValue.toString()));
                flowerBuilder.withWatering(measureAttribute);
                break;

            case MULTIPLYING:
                flowerBuilder.withMultiplying(Flower.Multiplying.fromString(elementValue.toString()));
                break;

            default:
                break;
        }
    }

    @Override
    public List<Flower> parseFromFile(boolean validate) throws XmlException {
        if (xmlFileName == null) {
            throw new IllegalArgumentException("XML file name can't be null");
        }

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser;

		if (validate && !isXMLValid("input.xsd", xmlFileName)) {
			throw new XmlException("Input xml file is not valid!", new IllegalArgumentException());
		}

        try {
            parser = factory.newSAXParser();
            parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            parser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            parser.parse(new File(xmlFileName), this);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new XmlException("Error occurred while parsing xml file " + xmlFileName, e);
        }

        return new ArrayList<>(flowers);
    }
}