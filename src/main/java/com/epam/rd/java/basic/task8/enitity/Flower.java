package com.epam.rd.java.basic.task8.enitity;

import java.util.Arrays;

public final class Flower {
    private final String name;
    private final Soil soil;
    private final String origin;
    private final String stemColour;
    private final String leafColour;
    private final Measure<Integer> aveLenFlower;
    private final Measure<Integer> temperature;
    private final Lighting lighting;
    private final Measure<Integer> watering;
    private final Multiplying multiplying;

    private Flower(FlowerBuilder builder) {
        this.name = builder.name;
        this.soil = builder.soil;
        this.origin = builder.origin;
        this.stemColour = builder.stemColour;
        this.leafColour = builder.leafColour;
        this.aveLenFlower = builder.aveLenFlower;
        this.temperature = builder.temperature;
        this.lighting = builder.lighting;
        this.watering = builder.watering;
        this.multiplying = builder.multiplying;
    }

    public String getName() {
        return name;
    }

    public Soil getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public Measure<Integer> getAveLenFlower() {
        return aveLenFlower;
    }

    public Measure<Integer> getTemperature() {
        return temperature;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public Measure<Integer> getWatering() {
        return watering;
    }

    public Multiplying getMultiplying() {
        return multiplying;
    }

    public enum Soil {
        PODZOLIC("подзолистая"),
        GROUND("грунтовая"),
        SOD_PODZOLIC("дерново-подзолистая");

        String value;

        Soil(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static Soil fromString(String s) throws IllegalArgumentException {
            return Arrays.stream(Soil.values())
                    .filter(v -> v.value.equals(s))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Can't parse Soil: " + s));
        }
    }

    public enum Multiplying {
        LEAVES("листья"),
        CUTTINGS("черенки"),
        SEEDS("семена");

        String value;

        Multiplying(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static Multiplying fromString(String s) throws IllegalArgumentException {
            return Arrays.stream(Multiplying.values())
                    .filter(v -> v.value.equals(s))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Can't parse Multiplying: " + s));
        }
    }

    public enum Lighting {
        YES("yes"),
        NO("no");

        String value;

        Lighting(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static Lighting fromString(String s) throws IllegalArgumentException {
            return Arrays.stream(Lighting.values())
                    .filter(v -> v.value.equals(s))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Can't parse Lighting: " + s));
        }
    }

    public static final class Measure<T extends Number> {
        private String unit;
        private T value;

        public Measure(String name, T value) {
            this.unit = name;
            this.value = value;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }
    }

    public static class FlowerBuilder {
        private String name;
        private Soil soil;
        private String origin;
        private String stemColour;
        private String leafColour;
        private Measure<Integer> aveLenFlower;
        private Measure<Integer> temperature;
        private Lighting lighting;
        private Measure<Integer> watering;
        private Multiplying multiplying;

        public Flower build() {
            return new Flower(this);
        }

        public FlowerBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public FlowerBuilder withSoil(Soil soil) {
            this.soil = soil;
            return this;
        }

        public FlowerBuilder withOrigin(String origin) {
            this.origin = origin;
            return this;
        }

        public FlowerBuilder withStemColour(String stemColour) {
            this.stemColour = stemColour;
            return this;
        }

        public FlowerBuilder withLeafColour(String leafColour) {
            this.leafColour = leafColour;
            return this;
        }

        public FlowerBuilder withAveLenFlower(Measure<Integer> aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
            return this;
        }

        public FlowerBuilder withTemperature(Measure<Integer> temperature) {
            this.temperature = temperature;
            return this;
        }

        public FlowerBuilder withLighting(Lighting lighting) {
            this.lighting = lighting;
            return this;
        }

        public FlowerBuilder withWatering(Measure<Integer> watering) {
            this.watering = watering;
            return this;
        }

        public FlowerBuilder withMultiplying(Multiplying multiplying) {
            this.multiplying = multiplying;
            return this;
        }

    }
}
