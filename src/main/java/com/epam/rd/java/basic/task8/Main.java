package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.enitity.Flower;
import com.epam.rd.java.basic.task8.util.Sorter;

import java.nio.file.Path;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> flowers = domController.parseFromFile(true);
		// sort (case 1)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByName(flowers);
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		DOMController.writeToFile(flowers, Path.of(outputXmlFile));
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowers = saxController.parseFromFile(true);
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByNeededTemperatureFromHighest(flowers);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		DOMController.writeToFile(flowers, Path.of(outputXmlFile));
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowers = staxController.parseFromFile(true);
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByNeededTemperatureFromHighestAndName(flowers);
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		DOMController.writeToFile(flowers, Path.of(outputXmlFile));
	}

}
