package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.enitity.Flower;
import com.epam.rd.java.basic.task8.exception.XmlException;
import com.epam.rd.java.basic.task8.util.FlowerXmlNamespace;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.util.FlowerXmlNamespace.*;

/**
 * Controller for StAX parser.
 */
public class STAXController implements XMLParser<Flower> {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public List<Flower> parseFromFile(boolean validate) throws XmlException {
        if (xmlFileName == null) {
            throw new IllegalArgumentException("XML file name can't be null");
        }

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		xmlInputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		xmlInputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		if (validate && !isXMLValid("input.xsd", xmlFileName)) {
			throw new XmlException("Input xml file is not valid!", new IllegalArgumentException());
		}
        XMLEventReader reader;
        try {
            reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
        } catch (XMLStreamException | FileNotFoundException e) {
            throw new XmlException("Can't obtain xml reader for parsing " + xmlFileName, e);
        }

        List<Flower> flowers = new ArrayList<>();
        Flower.FlowerBuilder flowerBuilder = new Flower.FlowerBuilder();

        try {
            while (reader.hasNext()) {
                XMLEvent nextEvent;
                nextEvent = reader.nextEvent();
                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    switch (FlowerXmlNamespace.fromString(startElement.getName().getLocalPart())) {
						case FLOWER:
							nextEvent = reader.nextEvent();
                            flowerBuilder = new Flower.FlowerBuilder();
                            break;

						case NAME:
							nextEvent = reader.nextEvent();
							flowerBuilder.withName(nextEvent.asCharacters().getData());
							break;

						case SOIL:
							nextEvent = reader.nextEvent();
							flowerBuilder.withSoil(Flower.Soil.fromString(nextEvent.asCharacters().getData()));
							break;

						case ORIGIN:
							nextEvent = reader.nextEvent();
							flowerBuilder.withOrigin(nextEvent.asCharacters().getData());
							break;

						case STEM_COLOUR:
							nextEvent = reader.nextEvent();
							flowerBuilder.withStemColour(nextEvent.asCharacters().getData());
							break;

						case LEAF_COLOUR:
							nextEvent = reader.nextEvent();
							flowerBuilder.withLeafColour(nextEvent.asCharacters().getData());
							break;

						case AVE_LEN_FLOWER:
							nextEvent = reader.nextEvent();
							flowerBuilder.withAveLenFlower(new Flower.Measure<>(
									startElement.getAttributeByName(new QName(MEASURE.getValue())).getValue(),
									Integer.valueOf(nextEvent.asCharacters().getData())
							));
							break;

						case TEMPERATURE:
							nextEvent = reader.nextEvent();
							flowerBuilder.withTemperature(new Flower.Measure<>(
									startElement.getAttributeByName(new QName(MEASURE.getValue())).getValue(),
									Integer.valueOf(nextEvent.asCharacters().getData())
							));
							break;

						case LIGHTING:
							nextEvent = reader.nextEvent();
							flowerBuilder.withLighting(Flower.Lighting.fromString(
									startElement.getAttributeByName(new QName(LIGHT_REQUIRING.getValue())).getValue()));
							break;

						case WATERING:
							nextEvent = reader.nextEvent();
							flowerBuilder.withWatering(new Flower.Measure<>(
									startElement.getAttributeByName(new QName(MEASURE.getValue())).getValue(),
									Integer.valueOf(nextEvent.asCharacters().getData())
							));
							break;

						case MULTIPLYING:
							nextEvent = reader.nextEvent();
							flowerBuilder.withMultiplying(Flower.Multiplying.fromString(nextEvent.asCharacters().getData()));
							break;

						default:
							break;
                    }
                }
                if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals(FLOWER.getValue())) {
						flowers.add(flowerBuilder.build());
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new XmlException("Exception occurred while reading events in xml file", e);
        }

		return flowers;

    }
}