package com.epam.rd.java.basic.task8.exception;

public class XmlException extends Exception {

    public XmlException(String message, Throwable cause) {
        super(message, cause);
    }

}
