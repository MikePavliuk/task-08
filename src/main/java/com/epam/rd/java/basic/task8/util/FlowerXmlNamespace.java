package com.epam.rd.java.basic.task8.util;

import java.util.Arrays;

public enum FlowerXmlNamespace {
    XMLNS("http://www.nure.ua"),
    XMLNS_XSI("http://www.w3.org/2001/XMLSchema-instance"),
    XSI_SCHEMA_LOCATION("http://www.nure.ua input.xsd "),
    FLOWERS("flowers"),
    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    VISUAL_PARAMETERS("visualParameters"),
    STEM_COLOUR("stemColour"),
    LEAF_COLOUR("leafColour"),
    AVE_LEN_FLOWER("aveLenFlower"),
    GROWING_TIPS("growingTips"),
    TEMPERATURE("tempreture"),
    LIGHTING("lighting"),
    WATERING("watering"),
    MULTIPLYING("multiplying"),
    MEASURE("measure"),
    LIGHT_REQUIRING("lightRequiring");

    private final String value;

    FlowerXmlNamespace(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static FlowerXmlNamespace fromString(String s) throws IllegalArgumentException {
        return Arrays.stream(FlowerXmlNamespace.values())
                .filter(v -> v.value.equals(s))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown value: " + s));
    }
}
