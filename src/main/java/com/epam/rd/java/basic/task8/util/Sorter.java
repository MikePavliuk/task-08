package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.enitity.Flower;

import java.util.Comparator;
import java.util.List;

public final class Sorter {

    public static final Comparator<Flower> compareFlowersName = Comparator.comparing(Flower::getName);
    public static final Comparator<Flower> compareFlowersByNeededTemperatureFromHighest = Comparator.comparing(o -> o.getTemperature().getValue(), Comparator.reverseOrder());
    public static final Comparator<Flower> compareFlowersByNeededTemperatureFromHighestAndThenByName = compareFlowersByNeededTemperatureFromHighest.thenComparing(compareFlowersName);

    private Sorter() {}

    public static void sortFlowersByName(final List<Flower> flowers) {
        flowers.sort(compareFlowersName);
    }

    public static void sortFlowersByNeededTemperatureFromHighest(final List<Flower> flowers) {
        flowers.sort(compareFlowersByNeededTemperatureFromHighest);
    }

    public static void sortFlowersByNeededTemperatureFromHighestAndName(final List<Flower> flowers) {
        flowers.sort(compareFlowersByNeededTemperatureFromHighestAndThenByName);
    }

}
