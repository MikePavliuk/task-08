package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.enitity.Flower;
import com.epam.rd.java.basic.task8.exception.XmlException;
import com.epam.rd.java.basic.task8.util.FlowerXmlNamespace;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController implements XMLParser<Flower> {

    private String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public List<Flower> parseFromFile(boolean validate) throws XmlException {
        if (xmlFileName == null) {
            throw new IllegalArgumentException("XML file name can't be null");
        }

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        if (validate && !isXMLValid("input.xsd", xmlFileName)) {
            throw new XmlException("Input xml file is not valid!", new IllegalArgumentException());
        }
        try {
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        } catch (ParserConfigurationException e) {
            throw new XmlException("Can't process xml securely", e);
        }

        List<Flower> flowersList = new ArrayList<>();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(xmlFileName));
            doc.getDocumentElement().normalize();
            NodeList flowers = doc.getElementsByTagName(FlowerXmlNamespace.FLOWER.getValue());
            for (int j = 0; j < flowers.getLength(); ++j) {

                Node node = flowers.item(j);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element flowerElement = (Element) node;
					Element visualParametersElement = (Element) flowerElement.getElementsByTagName(FlowerXmlNamespace.VISUAL_PARAMETERS.getValue()).item(0);
                    Element growingTipsElement = (Element) flowerElement.getElementsByTagName(FlowerXmlNamespace.GROWING_TIPS.getValue()).item(0);

                    Flower flower = new Flower.FlowerBuilder()
                            .withName(flowerElement.getElementsByTagName(FlowerXmlNamespace.NAME.getValue()).item(0)
                                    .getTextContent())

                            .withSoil(Flower.Soil.fromString(
                                    flowerElement.getElementsByTagName(FlowerXmlNamespace.SOIL.getValue()).item(0)
                                            .getTextContent()))

                            .withOrigin(flowerElement.getElementsByTagName(FlowerXmlNamespace.ORIGIN.getValue())
                                    .item(0)
                                    .getTextContent())

                            .withStemColour(visualParametersElement.getElementsByTagName(FlowerXmlNamespace.STEM_COLOUR.getValue()).item(0)
                                    .getTextContent())

                            .withLeafColour(visualParametersElement.getElementsByTagName(FlowerXmlNamespace.LEAF_COLOUR.getValue()).item(0)
                                    .getTextContent())

                            .withAveLenFlower(new Flower.Measure<>(
                                    visualParametersElement.getElementsByTagName(FlowerXmlNamespace.AVE_LEN_FLOWER.getValue()).item(0)
                                            .getAttributes()
                                            .getNamedItem(FlowerXmlNamespace.MEASURE.getValue())
                                            .getTextContent(),
                                    Integer.valueOf(visualParametersElement.getElementsByTagName(FlowerXmlNamespace.AVE_LEN_FLOWER.getValue()).item(0)
                                            .getTextContent())))

                            .withTemperature(new Flower.Measure<>(
                                    growingTipsElement.getElementsByTagName(FlowerXmlNamespace.TEMPERATURE.getValue()).item(0)
                                            .getAttributes()
                                            .getNamedItem(FlowerXmlNamespace.MEASURE.getValue())
                                            .getTextContent(),
                                    Integer.valueOf(growingTipsElement.getElementsByTagName(FlowerXmlNamespace.TEMPERATURE.getValue()).item(0)
                                            .getTextContent())))

                            .withLighting(Flower.Lighting.fromString(
                                    growingTipsElement.getElementsByTagName(FlowerXmlNamespace.LIGHTING.getValue()).item(0)
                                            .getAttributes()
                                            .getNamedItem(FlowerXmlNamespace.LIGHT_REQUIRING.getValue())
                                            .getTextContent()))

                            .withWatering(new Flower.Measure<>(
                                    growingTipsElement.getElementsByTagName(FlowerXmlNamespace.WATERING.getValue()).item(0)
                                            .getAttributes()
                                            .getNamedItem(FlowerXmlNamespace.MEASURE.getValue())
                                            .getTextContent(),
                                    Integer.valueOf(growingTipsElement.getElementsByTagName(FlowerXmlNamespace.WATERING.getValue()).item(0)
                                            .getTextContent())))

                            .withMultiplying(Flower.Multiplying.fromString(
                                    flowerElement.getElementsByTagName(FlowerXmlNamespace.MULTIPLYING.getValue()).item(0)
                                            .getTextContent()))
                            .build();

                    flowersList.add(flower);
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new XmlException("Can't parse xml file with name " + xmlFileName, e);
        }

        return new ArrayList<>(flowersList);
    }

    public static void writeToFile(List<Flower> flowers, Path path) throws XmlException {
        if (flowers == null) {
            throw new XmlException("Can't write to file not parsed list", new NullPointerException());
        }

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new XmlException("Can't obtain new document builder from " + dbf, e);
        }
        Document document = db.newDocument();

        Element flowersRoot = document.createElement(FlowerXmlNamespace.FLOWERS.getValue());
        flowersRoot.setAttribute("xmlns", FlowerXmlNamespace.XMLNS.getValue());
        flowersRoot.setAttribute("xmlns:xsi", FlowerXmlNamespace.XMLNS_XSI.getValue());
        flowersRoot.setAttribute("xsi:schemaLocation", FlowerXmlNamespace.XSI_SCHEMA_LOCATION.getValue());
        document.appendChild(flowersRoot);

        for (Flower item : flowers) {
            Element flower = document.createElement(FlowerXmlNamespace.FLOWER.getValue());
            flowersRoot.appendChild(flower);

            Element name = document.createElement(FlowerXmlNamespace.NAME.getValue());
            name.setTextContent(item.getName());
            flower.appendChild(name);

            Element soil = document.createElement(FlowerXmlNamespace.SOIL.getValue());
            soil.setTextContent(item.getSoil().getValue());
            flower.appendChild(soil);

            Element origin = document.createElement(FlowerXmlNamespace.ORIGIN.getValue());
            origin.setTextContent(item.getOrigin());
            flower.appendChild(origin);

            Element visualParameters = document.createElement(FlowerXmlNamespace.VISUAL_PARAMETERS.getValue());
            flower.appendChild(visualParameters);

            Element stemColour = document.createElement(FlowerXmlNamespace.STEM_COLOUR.getValue());
            stemColour.setTextContent(item.getStemColour());
            visualParameters.appendChild(stemColour);

            Element leafColour = document.createElement(FlowerXmlNamespace.LEAF_COLOUR.getValue());
            leafColour.setTextContent(item.getLeafColour());
            visualParameters.appendChild(leafColour);

            Element aveLenFlower = document.createElement(FlowerXmlNamespace.AVE_LEN_FLOWER.getValue());
            aveLenFlower.setTextContent(String.valueOf(item.getAveLenFlower().getValue()));
            aveLenFlower.setAttribute(FlowerXmlNamespace.MEASURE.getValue(), item.getAveLenFlower().getUnit());
            visualParameters.appendChild(aveLenFlower);

            Element growingTips = document.createElement(FlowerXmlNamespace.GROWING_TIPS.getValue());
            flower.appendChild(growingTips);

            Element temperature = document.createElement(FlowerXmlNamespace.TEMPERATURE.getValue());
            temperature.setTextContent(String.valueOf(item.getTemperature().getValue()));
            temperature.setAttribute(FlowerXmlNamespace.MEASURE.getValue(), item.getTemperature().getUnit());
            growingTips.appendChild(temperature);

            Element lighting = document.createElement(FlowerXmlNamespace.LIGHTING.getValue());
            lighting.setAttribute(FlowerXmlNamespace.LIGHT_REQUIRING.getValue(), item.getLighting().getValue());
            growingTips.appendChild(lighting);

            Element watering = document.createElement(FlowerXmlNamespace.WATERING.getValue());
            watering.setTextContent(String.valueOf(item.getWatering().getValue()));
            watering.setAttribute(FlowerXmlNamespace.MEASURE.getValue(), item.getWatering().getUnit());
            growingTips.appendChild(watering);

            Element multiplying = document.createElement(FlowerXmlNamespace.MULTIPLYING.getValue());
            multiplying.setTextContent(item.getMultiplying().getValue());
            flower.appendChild(multiplying);
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            throw new XmlException("Can't obtain transformer for creating xml file", e);
        }
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(path.toFile());
        try {
            transformer.transform(source, result);
        } catch (TransformerException e) {
            throw new XmlException("Can't transform created document to written xml file", e);
        }

    }

}
